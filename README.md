# week4

Lesson November 30, 2022

1. map of geo-tagged data: [notebok](./scripts/map_geotagged_info.ipynb), [html](./slides/map_geotagged_info.html) and [results](./results/map_geotagged.html) 

use cases:

2. movie review topics with respect to sentiments: [notebook](./use_cases/sentiment_analysis_topic_models.ipynb) and [html](./slides/sentiment_analysis_topic_models.html)
3. movie review sentiment interpretation: [notebook](./use_cases/sentiment_analysis_model_interpretation.ipynb) and [html](./slides/sentiment_model_interpretation.html)
4. movie review sentiment analysis (unsupervised): [notebook](./use_cases/sentiment_analysis_unsupervised_lexical.ipynb) and [pdf](./slides/sentiment_analysis_unsupervised_lexical.pdf)
5. movie review sentiment analysis (supervised): [notebook](./use_cases/sentiment_analysis_supervised.ipynb) and [pdf](./slides/sentiment_analysis_supervised.pdf)

Lesson December 1, 2022

1. map of geo-tagged volcano data: [notebok](./solution/map_geotagged_volcano_info.ipynb), [html](./slides/map_geotagged_volcano_info.html) and [results](./results/map_geotagged_volcano.html)  

**NOTE**: the `plugin` parameter gets a text to be shown on the marker. Its value can be managed as a `string`, e.g. `f'Country: {label}'` or `f'Location: {label}; Name: {name}'`.

2. hierarchical clustering with similarity features: [notebook](./scripts/hierarchical_clustering_text_feature.ipynb), and [pdf](./slides/hierarchical_clustering_text_feature.ipynb)
3. text classification: [notebook](./scripts/text_classification.ipynb) and [pdf](./slides/text_classification.pdf)
4. classification performance: [notebook](./scripts/classification_performance_evaluation_metrics.ipynb), and [pdf](./slides/classification_performance_evaluation_metrics.ipynb)
5. performance metrics: [pdf](./slides/performance_metrics.ipynb)

use cases:

6. titanic classifier: [notebook](./use_cases/titanic_classifier.ipynb) and [pdf](./slides/titanic_classifier.pdf)
7. california house price prediction: [notebook](./use_cases/california_housing_price_classifier.ipynb) and [pdf](./slides/california_housing_price_classifier.pdf)




[homework3 solution](https://gitlab.com/90477_mls_4ds_ii/homework3_solution)

[homework4](https://gitlab.com/90477_mls_4ds_ii/homework4)
